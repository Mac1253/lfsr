#include <iostream>

using namespace std;
unsigned char *Crypt(unsigned char *data, int dataLength, unsigned int initialValue) {

     unsigned long int b;
      for (int i = 0; i < dataLength; i++) {
        b = initialValue & 1;
        initialValue >>= 1; // shift bits
        b = (b >> 1) ^ initialValue; 
        if (b) { 
          // shift bit
          initialValue ^= 0xB400u;
          }
    }
    return data;
    
}


int main() {
    unsigned char data[] = {'a', 'p', 'p', 'l', 'e'};
    int dataLength = 5;
    unsigned int ini = 0x12345678;
    
    unsigned char *result[] = {Crypt(data, dataLength, ini)};
    cout << result << '\n';
    unsigned char *decrypt = {Crypt(*result, dataLength, ini)};
    cout << decrypt;
    cin.get();
}
